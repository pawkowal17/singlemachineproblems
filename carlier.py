from copy import deepcopy
from schrage import schrage
from preschrage import preschrage


def carlier(size_list, task_list, cmax, best_pi):

    task_list1 = deepcopy(task_list)
    ub, pi = schrage(task_list1)

    if ub < cmax:
        cmax = ub
        best_pi = pi

    a, b = critical_path(size_list, pi, ub)
    exist, c = inter_task(pi, a, b)

    if not exist:
        return cmax, best_pi

    r_prim = 1000000
    q_prim = 0
    p_prim = 0

    for i in range(c+1, b):
        r_prim = min(r_prim, pi[i].t_r)

        q_prim = min(q_prim, pi[i].t_q)

        p_prim += pi[i].t_p

    saved_r = pi[c].t_r
    pi[c].t_r = max(pi[c].t_r, r_prim + p_prim)

    pi1 = deepcopy(pi)
    lb = preschrage(pi1)
    lb = max(lb, r_prim + p_prim + q_prim)
    if lb < cmax:
        cmax, best_pi = carlier(size_list, pi, cmax, best_pi)
    pi[c].t_r = saved_r

    saved_q = pi[c].t_q
    pi[c].t_q = max(pi[c].t_q, q_prim + p_prim)

    pi2 = deepcopy(pi)
    lb = preschrage(pi2)
    lb = max(lb, r_prim + p_prim + q_prim)
    if lb < cmax:
        cmax, best_pi = carlier(size_list, pi, cmax, best_pi)
    pi[c].t_q = saved_q

    return cmax, best_pi


def critical_path(size_list, task_list, cmax):

    b, a = 0, 0

    for i in range(0, size_list):
        if cmax == task_list[i].t_on + task_list[i].t_p + task_list[i].t_q:
            b = max(b, i)

    for j in range(0, b):
        sum_p = 0
        for s in range(j, b):
            sum_p += task_list[s].t_p
        if cmax == task_list[j].t_r + task_list[b].t_q + sum_p:
            a = max(a, j)

    return a, b


def inter_task(task_list, a, b):

    c = 0
    found = False

    for i in range(a, b):
        if task_list[i].t_q < task_list[b].t_q:
            c = max(c, i)
            found = True

    return found, c
