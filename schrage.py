from Task import Task


def schrage(task_list):

    min_r = 1000000
    for task in task_list:
        if min_r > task.t_r:
            min_r = task.t_r

    t = 0
    cmax = 0
    k = 0
    ready_list = []
    sorted_list = []
    minimal = Task
    maximum = Task

    while task_list or ready_list:
        while task_list and min_r <= t:
            min_r = 1000000
            for task in task_list:
                if min_r > task.t_r:
                    min_r = task.t_r
                    minimal = task
            if min_r <= t:
                ready_list.append(minimal)
                task_list.remove(minimal)
        if not ready_list:
            min_r = 1000000
            for task in task_list:
                if min_r > task.t_r:
                    min_r = task.t_r
            t = min_r
        else:
            max_q = 0
            for task in ready_list:
                if max_q < task.t_q:
                    max_q = task.t_q
                    maximum = task
            sorted_list.append(maximum)
            ready_list.remove(maximum)
            maximum.t_on = t
            k += 1
            t += maximum.t_p
            cmax = max(cmax, t + maximum.t_q)

    # ready_list.clear()
    # sorted_list.clear()

    return cmax, sorted_list
