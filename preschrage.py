from copy import deepcopy
from Task import Task


def preschrage(task_list):

    min_r = 1000000
    for task in task_list:
        if min_r > task.t_r:
            min_r = task.t_r

    t = 0
    cmax = 0
    k = 0
    ready_list = []
    sorted_list = []
    # minimal = Task
    # maximum = Task
    l = Task(0, 0, 1000000)

    while task_list or ready_list:
        while task_list and min_r <= t:
            min_r, minimal = mini_r(task_list)
            if min_r <= t:
                ready_list.append(minimal)
                task_list.remove(minimal)
            if minimal.t_q > l.t_q and min_r < t:
                l.t_p = t - minimal.t_r
                t = minimal.t_r
                if l.t_p > 0:
                    ready_list.append(l)
        if not ready_list:
            min_r, minimal = mini_r(task_list)
            t = min_r
        else:
            max_q, maximum = maxi_q(ready_list)
            l = deepcopy(maximum)
            ready_list.remove(maximum)
            k += 1
            maximum.t_on = t
            sorted_list.append(maximum)
            t += maximum.t_p
            cmax = max(cmax, t + maximum.t_q)

    # ready_list.clear()
    # sorted_list.clear()

    return cmax


def mini_r(task_list):

    minimal = Task
    min_r = 1000000
    for task in task_list:
        if min_r > task.t_r:
            min_r = task.t_r
            minimal = task
    return min_r, minimal


def maxi_q(ready_list):

    maximum = Task
    max_q = 0
    for task in ready_list:
        if max_q < task.t_q:
            max_q = task.t_q
            maximum = task
    return max_q, maximum
