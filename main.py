#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from copy import deepcopy
from Task import Task
from schrage import schrage
from preschrage import preschrage
from carlier import carlier


in_list = ["SCHRAGE1.DAT", "SCHRAGE2.DAT", "SCHRAGE3.DAT", "SCHRAGE4.DAT",
           "SCHRAGE5.DAT", "SCHRAGE6.DAT", "SCHRAGE7.DAT", "SCHRAGE8.DAT", "SCHRAGE9.DAT"]
out_list = ["SCHRAGE1.OUT", "SCHRAGE2.OUT", "SCHRAGE3.OUT", "SCHRAGE4.OUT",
            "SCHRAGE5.OUT", "SCHRAGE6.OUT", "SCHRAGE7.OUT", "SCHRAGE8.OUT", "SCHRAGE9.OUT"]

for i in list(range(1)):
    with open(in_list[0], "r") as read:
        data = read.readlines()

    size_list = int(data[0])
    data.remove(data[0])
    task_list = []

    for line in data:
        numbers = line.split()
        task_list.append(Task(int(numbers[0]), int(numbers[1]), int(numbers[2])))
    read.close()

    task_list1 = deepcopy(task_list)
    task_list2 = deepcopy(task_list)
    task_list3 = deepcopy(task_list)

    print("schrage")
    cmax1, sorted_list = schrage(task_list1)
    print(cmax1)

    print("preschrage")
    cmax2 = preschrage(task_list2)
    print(cmax2)

    cmax3 = 1000000
    best_pi = []

    print("carlier")
    cmax3, best_pi = carlier(size_list, task_list3, cmax3, best_pi)
    print(cmax3)

    sorted_list.clear()
    best_pi.clear()
    task_list.clear()
    task_list1.clear()
    task_list2.clear()
    task_list3.clear()
